let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = ['258 Washington Ave NW','California',90011];
const [houseNumber,state,city] = address;
console.log(`I live in ${houseNumber},${state},${city}`);

let animal = {
	name: 'Lolong',
	type: 'Saltwater crocodile',
	weight: 1075,
	sizeFt: 20,
	sizeIn: 3 
}
const {name,type,weight,sizeFt,sizeIn} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${sizeFt} ft ${sizeIn}`);

let numbers = [1,2,3,4,5];
numbers.forEach((number) => {
	console.log(`${number}`);
})

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = 'Frankie';
myDog.age = 5;
myDog.breed = 'Miniature Dachshund';
console.log(myDog);


